package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

//function that creates the sound of the watch
class BeepingState implements StopwatchState{ // Beeping state and stopwatch state are two different classes. Implement is keyword that connects them
    public BeepingState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    // different methods that allow the stopwatch to pause and reset
    @Override
    public void onStartStop() {
        // onAction from stopped state increments the counter and calls to the waiting state
        sm.actionStop();
        sm.actionReset();
        sm.toStoppedState();
    }


    @Override
    public void onTick() {
        sm.actionBeep(); //access stopwatch adapter //calls into state machine that will play the beeping in stopwatch adapter
        //throw new UnsupportedOperationException("onTick");
    }

    //method that allows the stopwatch to update
    @Override
    public void updateView() {
        //sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.BEEPING;
    } //returns sound
}
