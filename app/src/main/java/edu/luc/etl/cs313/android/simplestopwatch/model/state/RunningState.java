package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.app.Activity;
import android.widget.EditText;

import edu.luc.etl.cs313.android.simplestopwatch.R;

//
class RunningState extends Activity implements StopwatchState{

    public RunningState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;


    //runningstate is a class that contains methods like starting the watch, countdown, and ontick and it allows
    //the stopwatch to constantly update, beep, and stop once it reaches its limit.
    @Override
    public void onStartStop() {
//        EditText text = (EditText)findViewById(R.id.seconds);
//        String value = text.getText().toString();
//        if (value != ""){
//            sm.actionBeep();
//        }
        sm.resetRuntime();
        sm.actionInc();
        sm.actionUpdateView();
        if(sm.getUITime() == 99 ) {
            sm.actionUpdateView();
            sm.actionBeep();
            sm.toCountDownState();
        }

    }

    @Override
    public void onTick() {
        sm.incRuntime(); //sm.actionDec
        if (sm.getRuntime() == 3 || sm.getUITime() == 99){ //Firstly, you click the bottom to add the number,
            // if you don’t click the button in 3 seconds, it will trigger the count down process.
            // max is 99
            // count state
            sm.actionUpdateView();
            sm.actionBeep();
            sm.toCountDownState();
        }
    }

    @Override
    public void updateView() { //view is updated
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}
