package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
//tests defaults stopwatch state machines
public class DefaultStopwatchStateMachine implements StopwatchStateMachine {

    public DefaultStopwatchStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;

    private final ClockModel clockModel;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private StopwatchState state;

    protected void setState(final StopwatchState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
    }

    private StopwatchUIUpdateListener uiUpdateListener;

    //private Beeper b; //added this

    @Override
    public void setUIUpdateListener(final StopwatchUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
        //b = uiUpdateListener.getBeeper(); //added this part too
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread

    //@Override public synchronized void onAction() { state.onAction(); }
    @Override public synchronized void onStartStop() {state.onStartStop();}
    @Override public synchronized void onTick()      { state.onTick(); }

    @Override public void updateUIRuntime() { uiUpdateListener.updateTime(timeModel.getUITime()); }


    //execution of the different functions such as pausing the watch, starting the timer, countdown,
    // and beeping.

    // known states
    private final StopwatchState STOPPED     = new StoppedState(this);
    private final StopwatchState RUNNING     = new RunningState(this);
    private final StopwatchState COUNTDOWN   = new CountDownState(this); // new LapStoppedState(this);
    private final StopwatchState BEEPING     = new BeepingState(this);

    // transitions
    @Override public void toRunningState()    { setState(RUNNING); }
    @Override public void toStoppedState()    { setState(STOPPED); }
    @Override public void toCountDownState()    { setState(COUNTDOWN); }
    @Override public void toBeepingState()    { setState(BEEPING); } //impliment this , fix errors, and it should work. stopped and countdown state to be fixed then it is close to done.


    // actions
    @Override public void actionInit()       { toStoppedState(); actionReset(); }
    @Override public void actionReset()      { timeModel.resetUITime(); actionUpdateView(); }
//    @Override public void actionWait()       { clockModel.clockWait(); }
    @Override public void actionStart()     {clockModel.start();}
    @Override public void actionStop()       { clockModel.stop(); }
    @Override public int getRuntime()        { return timeModel.getRuntime();}
    @Override public void resetRuntime()    {timeModel.resetRuntime(); actionUpdateView();}
    @Override public void incRuntime()  {timeModel.incRuntime();   actionUpdateView();}
    @Override public void decUITime() {timeModel.decUITime(); actionUpdateView();}
    @Override public int getUITime()      {return timeModel.getUITime();}  //getting the laptime
    @Override public void actionBeep() {uiUpdateListener.playDefaultNotification();}
    //@Override public void actionViewUpdate() {state.updateView();} //updates the view
    @Override public void actionInc()        {timeModel.incUITime(); }

    @Override public void actionUpdateView() { state.updateView(); }

}

