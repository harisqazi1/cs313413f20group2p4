package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

//countdown state and stopwatch state are two different classes that are connected
//by the usage of the keyword "implement"
class CountDownState implements StopwatchState {

    public CountDownState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    //allows the countdown time to pause and reset
    @Override
    public void onStartStop() {
        sm.actionStop();
        sm.actionReset();
        sm.toStoppedState();
    }

    //if the UI time is zero, the watch will update and beep
    @Override
    public void onTick() {
        sm.decUITime();
        sm.actionUpdateView();
        if (sm.getUITime() ==0){
            sm.actionUpdateView();
            sm.toBeepingState();
        }
        //throw new UnsupportedOperationException("onTick");
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.COUNTDOWN;
    }
}
