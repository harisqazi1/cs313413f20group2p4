package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class StoppedState implements StopwatchState {

    public StoppedState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        // onAction from stopped state increments the counter and calls to the waiting state
        sm.actionStart();
        sm.actionInc();
        sm.actionUpdateView(); //Changes the view
        sm.toRunningState(); //transition to running state

    }

    //In a class that takes clicks, when the user clocks the button, you have to get time time, and then if it is not 0, take the time they enter and go to that to the running state.
    @Override
    public void onTick() {throw new UnsupportedOperationException("onTick");} //{throw new UnsupportedOperationException("onTick");}


    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.STOPPED;
    }


}

// update the view and transitions from the stop state to the running state