package edu.luc.etl.cs313.android.simplestopwatch.model.state;

/**
 * The restricted view states have of their surrounding state machine.
 * This is a client-specific interface in Peter Coad's terminology.
 *
 * @author laufer
 */
interface StopwatchSMStateView {

    // transitions
    void toRunningState();
    void toStoppedState();
    void toCountDownState();
    void toBeepingState();


    // actions
    void actionInit();
    void actionReset();
    void actionStart();
    void actionStop();
    void actionInc();
    int getRuntime();
    void incRuntime();
    void actionBeep();
    void actionUpdateView();
    void resetRuntime();
    int getUITime();
    void decUITime();

    // state-dependent UI updates
    void updateUIRuntime();



}
//defines various transitions like reseting, starting, stopping, and incremeneting.