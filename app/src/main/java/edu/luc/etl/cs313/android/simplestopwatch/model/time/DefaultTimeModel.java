package edu.luc.etl.cs313.android.simplestopwatch.model.time;

/**
 * An implementation of the stopwatch data model.
 */
//double checks if the function starts at 0
public class DefaultTimeModel implements TimeModel {

    private int runningTime = 0;

    private int UITime = 0; //changed this from -1

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void resetUITime() { UITime = 0; } //added this

    @Override
    public void incRuntime() {runningTime++;}

    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override
    public void incUITime() {
        UITime = UITime + 1; //Adding one to the value each time
    }

    @Override
    public void decUITime() {
        UITime = UITime - 1; //Adding one to the value each time
    }

    @Override
    public int getUITime() {
        return UITime;
    }
}